#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	unsigned char key[KLEN_SKE * 2];
	// generate key using entropy if given, otherwise use randbytes
	if (entropy){
		HMAC(EVP_sha512(),KDF_KEY,HM_LEN,entropy,entLen,key,NULL);
	}
	else{
		randBytes(key,KLEN_SKE * 2);	
	}

	// use first half for HMAC key and second half for AES key
	memcpy(K->hmacKey,key,KLEN_SKE);
	memcpy(K->aesKey,key+KLEN_SKE,KLEN_SKE);
	return 0;
}	
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	// create IV if null
	int IV_created = 0;
	if (!IV){
		IV = (unsigned char*) malloc(AES_BLOCK_SIZE);
		randBytes(IV,AES_BLOCK_SIZE);
		IV_created = 1;
	}

	unsigned char ct[len], hmac[HM_LEN];
	int nWritten;

	// encrypt using AES-CTR with given AES key
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_EncryptUpdate(ctx,ct,&nWritten,inBuf,len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);

	// hash the cipher text with IV for verification purposes
	unsigned char iv_ct[len+AES_BLOCK_SIZE];
	memcpy(iv_ct,IV,AES_BLOCK_SIZE);
	memcpy(iv_ct+AES_BLOCK_SIZE,ct,len);
	HMAC(EVP_sha256(),K->hmacKey,HM_LEN,iv_ct,len+AES_BLOCK_SIZE,hmac,NULL);

	// append everything together for cipher text
	memcpy(outBuf,IV,AES_BLOCK_SIZE);
	memcpy(outBuf+AES_BLOCK_SIZE,ct,nWritten);
	memcpy(outBuf+AES_BLOCK_SIZE+nWritten,hmac,HM_LEN);

	// if IV was malloced, then free it
	if (IV_created)
		free(IV);

	return ske_getOutputLen(nWritten); 
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	int fdin, fdout;
	size_t size;
	struct stat s;

	// open input file (plain text to encrypt)
	fdin = open(fnin,O_RDONLY);

	// get input size
	fstat(fdin,&s);
	size = s.st_size;

	// mmap the input file to f
	unsigned char* f = (unsigned char*) mmap(0,size,PROT_READ,MMAP_SEQ,fdin,0);

	// call ske_encrypt
	unsigned char outBuf[ske_getOutputLen(size)];
	size_t ct_len = ske_encrypt(outBuf,f,size,K,IV);

	// open output file
	if ((fdout = open(fnout,O_CREAT|O_WRONLY,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening file during encrypt: %s\n", strerror( errno ));
		perror("Error message");
	}

	// adjust offset
	if (-1==lseek(fdout,offset_out,SEEK_SET)){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error offsetting file during encrypt: %s\n", strerror( errno ));
		perror("Error message");
	}

	// write to output file
	if (-1==write(fdout,outBuf,ske_getOutputLen(size))){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error writing file during encrypt: %s\n", strerror( errno ));
		perror("Error message");
	}

	//deallocate stuff
	close(fdin);
	close(fdout);
	munmap(f,size);

	return ct_len;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	unsigned char IV[AES_BLOCK_SIZE], ct[len-AES_BLOCK_SIZE-HM_LEN], hmac[HM_LEN], test_hmac[HM_LEN];

	// separate input cipher text into different buffers
	memcpy(IV,inBuf,AES_BLOCK_SIZE);
	memcpy(ct,inBuf+AES_BLOCK_SIZE,len-AES_BLOCK_SIZE-HM_LEN);
	memcpy(hmac,inBuf+len-HM_LEN,HM_LEN);
	int nWritten;

	// check if cipher text was altered in any way
	unsigned char iv_ct[len-HM_LEN];
	memcpy(iv_ct,inBuf,len-HM_LEN);
	HMAC(EVP_sha256(),K->hmacKey,HM_LEN,iv_ct,len-HM_LEN,test_hmac,NULL);
	if (memcmp(hmac,test_hmac,HM_LEN))
		return -1;

	// decrypt cipher text using AES-CTR with given AES key
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,ct,len-AES_BLOCK_SIZE-HM_LEN))
		ERR_print_errors_fp(stderr);

	return nWritten;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	int fdin, fdout;
	size_t size;
	struct stat s;

	// open input file
	fdin = open(fnin,O_RDONLY);

	// get input file size
	fstat(fdin,&s);
	size = s.st_size;

	// mmap input file to f
	unsigned char* f = (unsigned char*) mmap(0,size,PROT_READ,MMAP_SEQ,fdin,0);

	// account for offset in reading
	size -= offset_in;

	// call ske_decrypt with appropriate offset
	unsigned char outBuf[size-AES_BLOCK_SIZE-HM_LEN];
	size_t pt_len = ske_decrypt(outBuf,f+offset_in,size,K);

	// open output file
	if ((fdout = open(fnout,O_CREAT|O_WRONLY,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening file during decrypt: %s\n", strerror( errno ));
		perror("Error printed by perror");
	}
	// write to output file
	if (-1==write(fdout,outBuf,size-AES_BLOCK_SIZE-HM_LEN)){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error writing file during decrypt: %s\n", strerror( errno ));
		perror("Error printed by perror");
	}

	// deallocate stuff
	close(fdin);
	close(fdout);
	munmap(f,size);

	return pt_len;
}
